package br.well.eti.compasso.productms.mapper;

import br.well.eti.compasso.productms.model.Product;
import br.well.eti.compasso.productms.model.dto.request.ProductRequest;
import br.well.eti.compasso.productms.model.dto.response.ProductResponse;
import br.well.eti.compasso.productms.model.dto.search.ProductSearch;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper( ProductMapper.class );
    ProductResponse productToResponse(Product product);
    Product requestToProduct(ProductRequest productRequest);
    Product searchToProduct(ProductSearch productSearch);
}
