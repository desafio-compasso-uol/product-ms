package br.well.eti.compasso.productms.model.dto.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
public class ProductResponse {
    @ApiModelProperty(value = "Id do produto", example = "321")
    private Long id;
    @ApiModelProperty(value = "Nome do produto", example = "Livro ABC")
    private String name;
    @ApiModelProperty(value = "Descrição do produto", example = "Livro de Técnicas especiais ABC")
    private String description;
    @ApiModelProperty(value = "Preço doo produto", example = "15.50")
    private BigDecimal price;
}
