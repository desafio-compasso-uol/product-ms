package br.well.eti.compasso.productms.controllers;

import br.well.eti.compasso.productms.model.dto.request.ProductRequest;
import br.well.eti.compasso.productms.model.dto.response.ProductResponse;
import br.well.eti.compasso.productms.model.dto.search.ProductSearch;
import br.well.eti.compasso.productms.service.ProductService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {
    @Autowired
    private ProductService service;

    @PostMapping
    @ApiOperation(value = "Save new Product")
    public ResponseEntity<ProductResponse> save(@Valid @RequestBody ProductRequest body){
        return ResponseEntity.status(HttpStatus.CREATED).body(service.save(body));
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update Product by id")
    public ResponseEntity<ProductResponse> update(@Positive @PathVariable Long id, @Valid @RequestBody ProductRequest body){
        return ResponseEntity.ok().body(service.update(id, body));
    }

    @GetMapping
    @ApiOperation(value = "Get all Product")
    public ResponseEntity<List<ProductResponse>> getAll(ProductRequest request){
        return ResponseEntity.ok().body(service.getAll(request));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get Product by id")
    public ResponseEntity<ProductResponse> get(@PathVariable Long id){
        return ResponseEntity.ok().body(service.get(id));
    }

    @GetMapping("/search")
    @ApiOperation(value = "Get search Products by params")
    public ResponseEntity<List<ProductResponse>> getAllSearch(ProductSearch search){
        return ResponseEntity.ok().body(service.search(search));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete Product by id")
    public ResponseEntity delete(@Positive @PathVariable Long id){
        service.delete(id);
        return ResponseEntity.ok().build();
    }

}
