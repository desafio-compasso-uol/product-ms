package br.well.eti.compasso.productms.service;

import br.well.eti.compasso.productms.exception.NotFoundException;
import br.well.eti.compasso.productms.mapper.ProductMapper;
import br.well.eti.compasso.productms.model.Product;
import br.well.eti.compasso.productms.model.dto.request.ProductRequest;
import br.well.eti.compasso.productms.model.dto.response.ProductResponse;
import br.well.eti.compasso.productms.model.dto.search.ProductSearch;
import br.well.eti.compasso.productms.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductService {
    @Autowired
    private ProductRepository repository;

    public ProductResponse save(ProductRequest body){
        Product product = repository.save(ProductMapper.INSTANCE.requestToProduct(body));
        return ProductMapper.INSTANCE.productToResponse(product);
    }

    public ProductResponse update(Long id, ProductRequest body){
        return repository.findById(id)
                .map(product -> {
                    product.setName(body.getName());
                    product.setDescription(body.getDescription());
                    product.setPrice(body.getPrice());
                    return ProductMapper.INSTANCE.productToResponse(repository.save(product));
                })
                .orElseThrow(() -> new NotFoundException(id));
    }

    public ProductResponse get(Long id){
        return repository.findById(id).map(ProductMapper.INSTANCE::productToResponse)
                .orElseThrow(() -> new NotFoundException(id));
    }

    public List<ProductResponse> getAll(ProductRequest request){
        ExampleMatcher caseInsensitiveExampleMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
        Example<Product> example = Example.of(ProductMapper.INSTANCE.requestToProduct(request), caseInsensitiveExampleMatcher);
        return repository.findAll(example).stream().map(ProductMapper.INSTANCE::productToResponse).collect(Collectors.toList());
    }

    public List<ProductResponse> search(ProductSearch search){
        List<Product> productList = repository.findAll()
                .stream().filter(product ->
                        (search.getQ() != null && (product.getDescription().toLowerCase().contains(search.getQ()) || product.getName().toLowerCase().contains(search.getQ()))) &&
                        (search.getMin_price() != null && product.getPrice() >= Double.valueOf(search.getMin_price())) &&
                        (search.getMax_price() != null && product.getPrice() <= Double.valueOf(search.getMax_price()))
        ).collect(Collectors.toList());
        return productList.stream().map(ProductMapper.INSTANCE::productToResponse).collect(Collectors.toList());
    }

    public void delete(Long id){
        repository.deleteById(id);
    }
}
