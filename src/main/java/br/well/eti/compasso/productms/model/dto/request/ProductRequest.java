package br.well.eti.compasso.productms.model.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductRequest {
    @JsonProperty(value = "name")
    @ApiModelProperty(value = "Nome do produto", example = "Livro ABC")
    @NotNull(message = "Name cannot be null")
    @Length(min=3, max=50, message="Name must contain between 3 and 50 characters")
    private String name;
    @JsonProperty(value = "description")
    @ApiModelProperty(value = "Descrição do produto", example = "Livro de Técnicas especiais ABC")
    @NotNull(message = "Description cannot be null")
    @Length(min=10, max=150, message="Description must contain between 10 and 150 characters")
    private String description;
    @JsonProperty(value = "price")
    @ApiModelProperty(value = "Preço doo produto", example = "15.50")
    @NotNull(message = "Price cannot be null.")
    private Double price;
}
