package br.well.eti.compasso.productms.model.dto.search;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@RequiredArgsConstructor
public class ProductSearch {
    private String q;
    private String min_price;
    private String max_price;
}
