package br.well.eti.compasso.productms.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NotFoundException extends RuntimeException {
    private HttpStatus status_code = HttpStatus.NOT_FOUND;

    public NotFoundException(Long id) {
        super("Não foi possível encontrar registros com id:  " + id);
    }
}
