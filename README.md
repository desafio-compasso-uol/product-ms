# product-ms (Desafio Compasso UOL)

## Build Setup
```
# Run application
mvn spring-boot:run

# Endpoint URI
http://localhost:9999

# Swagger JSON
http://localhost:9999/v2/api-docs

# Swagger Documentation
http://localhost:9999/swagger-ui.html

```

## Objetivo
Neste microserviço deve ser possível criar, alterar, visualizar e excluir um determinado produto, além de visualizar a lista de produtos atuais disponíveis. Também deve ser possível realizar a busca de produtos filtrando por name, description e price.

### Endpoints

Devem ser disponibilizados os seguintes endpoints para operação do catálogo de produtos:


| Verbo HTTP  |  Resource path    |           Descrição           |
|-------------|:-----------------:|------------------------------:|
| POST        |  /products        |   Criação de um produto       |
| PUT         |  /products/{id}   |   Atualização de um produto   |
| GET         |  /products/{id}   |   Busca de um produto por ID  |
| GET         |  /products        |   Lista de produtos           |
| GET         |  /products/search |   Lista de produtos filtrados |
| DELETE      |  /products/{id}   |   Deleção de um produto       |


![](/gitlab/08.png)
![](/gitlab/01.png)
![](/gitlab/02.png)
![](/gitlab/03.png)
![](/gitlab/04.png)
![](/gitlab/05.png)
![](/gitlab/06.png)
![](/gitlab/07.png)